Diva Diamonds and Jewels features an endless selection of stunning diamonds, exquisite gems, and unique fine jewelry in a wide range of styles and prices. Our in-house jeweler has over 35 years of experience. Let us customize a perfect piece that you will cherish for a lifetime.

Address: 78 E San Francisco St, Santa Fe, NM 87501, USA

Phone: 505-988-1561

Website: https://www.divadiamondsjewelry.com

